# Changelog

## Unreleased
* Fix repository URL in `Cargo.toml`.
* Add additional documentation about GitLab dependency scanning.

## v0.2.0 (2021-03-27)
* Add additional information to the GitLab reports, such as the vulnerability title, description, and a summary of patched/unaffected versions.
* Add version metadata to vulnerabilities list.

## v0.1.0 (2021-03-21)
* Initial support for [GitLab dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html).
