FROM rust:1.50 as builder
ADD . /src
WORKDIR /src
RUN cargo build --release

FROM debian:stable-slim
RUN apt-get update && apt-get install -y openssl ca-certificates
COPY --from=builder /src/target/release/main /usr/bin/cargo-report
CMD ["/usr/bin/cargo-report"]