# cargo-report
[![Build Status](https://gitlab.com/cargo-report/cargo-report/badges/master/pipeline.svg?key_text=build)](https://gitlab.com/cargo-report/cargo-report/)
[![Documentation](https://docs.rs/cargo-report/badge.svg)](https://docs.rs/cargo-report/)

*Generate reports for integration with external software.*

## Features
Supported integrations:
* **GitLab Dependency Scan:** report vulnerable crates using the [RustSec advisory database](https://github.com/RustSec/advisory-db). This [GitLab feature](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) currently requires GitLab Ultimate.

### GitLab Dependency Scan
Recursively scan a directory for all `Cargo.lock` files. Then, use the `rustsec` crate to evaluate all dependencies against the [RustSec advisory database](https://github.com/RustSec/advisory-db). Dependencies which are found to be vulnerable will be included in a [GitLab dependency report](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html) named `gl-dependency-scanning.json` in the base directory.

To enable dependency scanning in your GitLab CI pipelines, add the following to `.gitlab-ci.yml`. Note, this will always use the latest released version of `cargo-report`. You may pin to a specific version by changing `:latest` to your desired version (for example, `:0.2.0`).
```yaml
scan-dependencies:
  stage: test
  allow_failure: true
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning.json
  image: registry.gitlab.com/cargo-report/cargo-report:latest
  script:
    - cargo-report gitlab-dependencies
```

## Contributing
* Write commit messages using the [Conventional Commits format](https://www.conventionalcommits.org/en/v1.0.0/).
* Open a merge request against the `master` branch.

## License
This software is licensed under the terms of the MIT license or the Apache License (Version 2.0), at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you shall be dual licensed as above, without any additional terms or conditions.
