//! Utilities for generating reports for integrating with external tools.
//!
//! Currently supported report categories are:
//! * Dependency reports: output the dependencies that a crate has. These reports may also output
//!   information about whether the dependencies are outdated or known to be vulnerable.

pub mod dependencies;

#[macro_use]
extern crate serde;
#[macro_use]
extern crate thiserror;
