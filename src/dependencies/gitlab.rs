//! Types to support creation of the GitLab dependency report.

use std::{
    io,
    path::{Path, PathBuf, StripPrefixError},
    str::FromStr,
};

use clap::ArgMatches;
use rustsec::{advisory::id::Kind, lockfile::Lockfile, Database, Report};
use semver::Version;

static REPORT_SCHEMA_VERSION: &str = "2.0";

/// Error encountered creating a GitLab dependency report.
#[derive(Error, Debug)]
pub enum Error {
    #[error("failed to read a Cargo.lock file")]
    CargoLock(#[from] cargo_lock::Error),
    #[error("error working with the rustsec advisory database")]
    Rustsec(#[from] rustsec::Error),
    #[error("IO error")]
    IoError(#[from] io::Error),
    #[error("failed to compute relative path")]
    RelativePath(#[from] StripPrefixError),
    #[error("JSON serialization error")]
    Serde(#[from] serde_json::Error),
}

/// Settings for generating a GitLab dependency report.
pub struct Settings {
    /// The project root, which will be scanned to find `Cargo.lock` files.
    ///
    /// This will be set to the value of the `CI_PROJECT_DIR` environment variable.
    pub project_dir: PathBuf,
    /// Settings used by the `rustsec` crate when evaluating the vulnerability database.
    pub rustsec: rustsec::report::Settings,
}

impl Settings {
    pub fn from_args(args: &ArgMatches) -> Self {
        Settings {
            project_dir: args
                .value_of("project-dir")
                .expect("project-dir parameter is required")
                .into(),
            rustsec: rustsec::report::Settings {
                target_arch: args.value_of("rustsec-target-arch").map(|arch| {
                    FromStr::from_str(arch).expect("invalid RustSec target architecture")
                }),
                target_os: args
                    .value_of("rustsec-target-os")
                    .map(|os| FromStr::from_str(os).expect("invalid RustSec target OS")),
                severity: args.value_of("rustsec-target-os").map(|severity| {
                    FromStr::from_str(severity).expect("invalid RustSec target OS")
                }),
                ignore: Vec::new(),
                informational_warnings: Vec::new(),
                package_scope: None,
            },
        }
    }
}

/// Generate a GitLab dependency report.
///
/// 1. Scan the project directory configured in the provided settings for `Cargo.lock` files.
/// 2. Download the RustSec advisory database.
/// 3. Evaluate each `Cargo.lock` file against the advisory database, collecting vulnerabilities.
pub fn report(settings: &Settings) -> Result<(), Error> {
    let lockfiles = super::discover_lock_files(&settings.project_dir)
        .expect("failed to scan for Cargo.lock files");
    let database = super::download_rustsec_database("/tmp/rustsec-db/")
        .expect("failed to download rustsec advisory database");
    let mut scan_results = DependencyScan::default();

    for lockfile_path in lockfiles {
        let scan = scan_lock_file(&lockfile_path, &database, &settings)?;
        scan_results.merge(scan);
    }

    let output_path = {
        let mut path: PathBuf = settings.project_dir.clone();
        path.push("gl-dependency-scanning.json");
        path
    };
    Ok(write_report(output_path, &scan_results)?)
}

/// Generate a [`DependencyScan`] report for a single `Cargo.lock` file.
fn scan_lock_file(
    path: &Path,
    database: &Database,
    settings: &Settings,
) -> Result<DependencyScan, Error> {
    let lockfile = Lockfile::load(&path)?;
    let report = Report::generate(&database, &lockfile, &settings.rustsec);

    let relative_path = path.strip_prefix(&settings.project_dir)?;
    Ok(DependencyScan::from_report(report, relative_path))
}

/// Serialize a [`DependencyScan`] to the output file.
fn write_report<P: AsRef<Path>>(path: P, report: &DependencyScan) -> Result<(), io::Error> {
    let mut output_file = std::fs::File::create(path)?;
    serde_json::to_writer_pretty(&mut output_file, &report)?;
    Ok(())
}

/// Holds rustsec scan results in a format that [GitLab dependency scan format].
///
/// This struct is constructed with a [`rustsec::Report`] and the path of the `Cargo.lock` file.
///
/// [GitLab dependency scan format]: https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/dependency-scanning-report-format.json
#[derive(Serialize, Deserialize)]
pub struct DependencyScan {
    version: &'static str,
    dependency_files: Vec<DependencyFile>,
    vulnerabilities: Vec<Vulnerability>,
}

impl DependencyScan {
    /// Convert a [`rustsec::Report`] to a GitLab [`DependencyScan`].
    ///
    /// The `lockfile_path` argument is the relative path to the `Cargo.lock` file that was
    /// evaluated (relative to the `CI_PROJECT_DIR` environment variable).
    fn from_report(report: Report, lockfile_path: &Path) -> Self {
        let dependency_files = vec![DependencyFile {
            path: lockfile_path.to_owned(),
            package_manager: "cargo",
            dependencies: report
                .vulnerabilities
                .list
                .iter()
                .map(|vulnerability| Dependency {
                    package: Package {
                        name: vulnerability.package.name.to_string(),
                    },
                    version: vulnerability.package.version.clone(),
                })
                .collect(),
        }];

        let vulnerabilities = report
            .vulnerabilities
            .list
            .iter()
            .map(|vulnerability| Vulnerability {
                category: "dependency_scanning".into(),
                description: vulnerability.advisory.description.clone(),
                identifiers: Some(&vulnerability.advisory.id)
                    .iter()
                    .cloned() // Convert from Item=&&Id to Item=&Id
                    .chain(vulnerability.advisory.aliases.iter())
                    .filter_map(Identifier::from_rustsec_id)
                    .collect(),
                location: Location {
                    file: lockfile_path.to_owned(),
                    dependency: LocationDependency {
                        package: Package {
                            name: vulnerability.package.name.to_string(),
                        },
                        version: vulnerability.package.version.clone(),
                    },
                },
                name: vulnerability.advisory.title.clone(),
                severity: Severity::Unknown,
                solution: describe_solution(&vulnerability),
                scanner: Scanner {
                    id: "rustsec".to_string(),
                    name: "RustSec".to_string(),
                },
            })
            .collect();

        Self {
            version: REPORT_SCHEMA_VERSION,
            dependency_files,
            vulnerabilities,
        }
    }

    fn merge(&mut self, mut other: Self) {
        self.dependency_files
            .extend(other.dependency_files.drain(..));
        self.vulnerabilities.extend(other.vulnerabilities.drain(..));
    }
}

impl Default for DependencyScan {
    fn default() -> Self {
        Self {
            version: REPORT_SCHEMA_VERSION,
            dependency_files: Vec::new(),
            vulnerabilities: Vec::new(),
        }
    }
}

fn describe_solution(vulnerability: &rustsec::vulnerability::Vulnerability) -> Option<String> {
    let patched = (!vulnerability.versions.patched.is_empty()).then(|| {
        let versions: Vec<String> = vulnerability
            .versions
            .patched
            .iter()
            .map(|version| version.to_string())
            .collect();
        let version_list = versions.join(", ");
        format!(
            "Switch to a patched version of this crate: {}.",
            version_list
        )
    });

    let unaffected = (!vulnerability.versions.unaffected.is_empty()).then(|| {
        let versions: Vec<String> = vulnerability
            .versions
            .unaffected
            .iter()
            .map(|version| version.to_string())
            .collect();
        let version_list = versions.join(", ");
        format!("The following versions are unaffected: {}.", &version_list)
    });

    match (patched, unaffected) {
        (Some(p), Some(u)) => Some(format!("{} {}", p, u)),
        (Some(p), None) => Some(p),
        (None, Some(u)) => Some(u),
        (None, None) => None,
    }
}

#[derive(Serialize, Deserialize)]
struct DependencyFile {
    path: PathBuf,
    package_manager: &'static str,
    dependencies: Vec<Dependency>,
}

#[derive(Serialize, Deserialize)]
struct Dependency {
    package: Package,
    version: Version,
}

#[derive(Serialize, Deserialize)]
struct Vulnerability {
    category: String,
    description: String,
    identifiers: Vec<Identifier>,
    location: Location,
    name: String,
    severity: Severity,
    solution: Option<String>,
    scanner: Scanner,
}

#[derive(Serialize, Deserialize)]
struct Identifier {
    r#type: String,
    name: String,
    value: String,
    url: Option<String>,
}

impl Identifier {
    fn from_rustsec_id(id: &rustsec::advisory::Id) -> Option<Self> {
        match id.kind() {
            Kind::RUSTSEC => Some(Self {
                r#type: "rustsec".to_string(),
                name: id.as_str().to_string(),
                value: id.as_str().to_string(),
                url: id.url(),
            }),
            Kind::CVE => Some(Self {
                r#type: "cve".to_string(),
                name: id.as_str().to_string(),
                value: id.as_str().to_string(),
                url: id.url(),
            }),
            _ => None,
        }
    }
}

#[derive(Serialize, Deserialize)]
struct Location {
    file: PathBuf,
    dependency: LocationDependency,
}

#[derive(Serialize, Deserialize)]
struct LocationDependency {
    package: Package,
    version: rustsec::Version,
}

#[derive(Serialize, Deserialize)]
struct Package {
    name: String,
}

#[derive(Serialize, Deserialize)]
enum Severity {
    Critical,
    High,
    Medium,
    Low,
    Info,
    Unknown,
}

#[derive(Serialize, Deserialize)]
struct Scanner {
    id: String,
    name: String,
}
