//! Dependency reports export information about dependencies of a crate and whether those
//! dependencies are outdated or have known vulnerabilities.
use std::path::{Path, PathBuf};

use rustsec::repository::git::Repository;
use rustsec::Database;
use walkdir::WalkDir;

pub mod gitlab;

static RUSTSEC_ADVISORY_DB: &str = "https://github.com/RustSec/advisory-db.git";

fn download_rustsec_database<P: Into<PathBuf>>(path: P) -> Result<Database, rustsec::Error> {
    let repo = Repository::fetch(&RUSTSEC_ADVISORY_DB, path, true)?;
    Database::load_from_repo(&repo)
}

/// Recursively scan directors in `base_path` looking for `Cargo.lock` files.
///
/// Returns an error if an error is encountered while walking through directors.
fn discover_lock_files<P: AsRef<Path>>(base_path: P) -> Result<Vec<PathBuf>, walkdir::Error> {
    let mut lock_files = Vec::new();
    for walk_res in WalkDir::new(base_path) {
        match walk_res {
            Ok(entry) => {
                if !entry.file_type().is_file() {
                    continue;
                }
                if entry.file_name() == "Cargo.lock" {
                    lock_files.push(entry.path().into())
                }
            }
            Err(err) => return Err(err),
        }
    }

    Ok(lock_files)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn discover_lock_files_project() {
        let lockfiles = discover_lock_files(".").expect("failed to find Cargo.lock files");
        assert!(lockfiles.contains(&PathBuf::from("./Cargo.lock")));
    }
}
