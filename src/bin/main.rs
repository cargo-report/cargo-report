use clap::{Arg, SubCommand};

fn main() {
    let args = clap::App::new("cargo-report")
        .bin_name("cargo-report")
        .version("0.1.0")
        .about("Generate reports for integrating with external tools")
        .author("Austin Hartzheim <inbox@austinhartzheim.me>")
        .subcommand(
            SubCommand::with_name("gitlab-dependencies")
                .about("generates GitLab dependency report")
                .arg(
                    Arg::with_name("project-dir")
                        .required(true)
                        .takes_value(true)
                        .env("CI_PROJECT_DIR"),
                ),
        )
        .get_matches();

    match args.subcommand() {
        ("gitlab-dependencies", Some(sub_args)) => {
            let settings = cargo_report::dependencies::gitlab::Settings::from_args(sub_args);
            cargo_report::dependencies::gitlab::report(&settings)
                .expect("failed to generate report");
        }
        (command, _) => {
            println!("unsupported subcommand \"{}\"; try --help", command);
            std::process::exit(1);
        }
    };
}
